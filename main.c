//
// Created by lukas on 20.02.16.
//

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <stdbool.h>

int main(){
    if(SDL_Init(SDL_INIT_VIDEO)==-1)return 1;
    if(TTF_Init()==-1)return 1;

    SDL_Surface *screen = SDL_SetVideoMode(800, 600, 32, SDL_DOUBLEBUF);
	if(!screen)return 1;

    SDL_WM_SetCaption("SDL SHIT", "sdl test");

    TTF_Font *font = TTF_OpenFont("PoiretOne.ttf", 30);
    if(!font)return 1;
    
    SDL_Color color = {255, 255, 0};
    
    SDL_Surface *text = TTF_RenderText_Blended(font, "TEST SDL", color);
    if(!text)return 1;
    
    SDL_Rect pos;
    pos.x = 10;
    pos.y = 3;

    bool run = true;
    while(1){
		SDL_Event event;
		while(SDL_PollEvent(&event) && run){
			switch(event.type){
				case SDL_QUIT:
					run = false;
                break;
			}
		}
        SDL_BlitSurface(text, 0, screen, &pos);
        SDL_Flip(screen);
    }
    
    SDL_FreeSurface(text);
	TTF_CloseFont(font);
    TTF_Quit();
    SDL_Quit();
    return 0;
}
